# Project PBP I02 Group Assignment


## Group members:
- Indira Devi Rusvandy (2006488732)
- Benedictus Jevan Winata (2006519870)
- Bryant Tanujaya (2006519896)
- Alexander Caesario Bangun (2006519851)
- Ajie Restu Sanggusti (2006490081)
- Muhammad Aaqil Abdullah (2006489501)
- Nicholas Jonathan Kinandana (2006490106)

## Project Description:

redacted is a workspace web application that helps organizing your daily tasks and makes sure
your remote work gets finished on time. Create projects and invite your team members to keep you guys all on the same page.

## Modules:
1. Home
2. Accounts
3. Create Project
4. Join Project
5. Project Workspace
6. Task
7. Meeting

